package com.ridr;

import rmn.androidscreenlibrary.ASSL;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;

public class AboutActivity extends Activity {

	LinearLayout relative;

	Button backBtn;
	TextView about1Text, about2Text, about3Text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_activity);

		relative = (LinearLayout) findViewById(R.id.relative);
		new ASSL(AboutActivity.this, relative, 1134, 720, false);

		backBtn = (Button) findViewById(R.id.backBtn);
		about1Text = (TextView) findViewById(R.id.about1Text);
		about1Text.setTypeface(Data.regularFont(getApplicationContext()));
		about2Text = (TextView) findViewById(R.id.about2Text);
		about2Text.setTypeface(Data.regularFont(getApplicationContext()),
				Typeface.BOLD);
		about3Text = (TextView) findViewById(R.id.about3Text);
		about3Text.setTypeface(Data.regularFont(getApplicationContext()));

		
		String s1 = "Ridr";
		String s2 = " is the INSTANT taxi-hailing app serving the Surrey and Berkshire area. ";
		String s4 = "Request any registered hackney carriage driver within an 8 mile radius! No credit card registration required*!\n\n*When driver is less than 5 min away";


		Spannable span1 = new SpannableString(s1);
		span1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0,
				span1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		about1Text.setText("");

		about1Text.append(span1);
		about1Text.append(s2);

		about1Text.append(s4);

		backBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(R.anim.left_in, R.anim.left_out);
			}
		});

	}

	// The Activity's onStart method
		@Override
		protected void onStart() {
			super.onStart();
			// Set up the Flurry session
			FlurryAgent.onStartSession(this, Data.FLURRY_KEY);
		}

		// The Activity's onStop method
		@Override
		protected void onStop() {
			super.onStop();
			FlurryAgent.onEndSession(this);
		}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_in, R.anim.left_out);
		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ASSL.closeActivity(relative);
		System.gc();
	}

}
