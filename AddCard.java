package com.ridr;

import java.util.regex.Pattern;

import org.json.JSONObject;

import rmn.androidscreenlibrary.ASSL;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;

public class AddCard extends Activity {
	private LinearLayout relative;
	private Stripe stripe;
	private String[] month_spinner;
	private String[] year_spinner;
	private EditText cardNumber;
	private EditText cvv;
	private EditText month;
	private EditText year;
	private Button signUpBtn;
	private TextView extraTextForScroll;
	protected String cvvString;
	protected String cardNumberString;
	private Button backBtn;
	TextView favTitleText;

	TextView cardNo, cardText;
	ImageView cardDivider;

	static final Pattern CODE_PATTERN = Pattern
			.compile("^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35{3}){11})$");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.add_card_screen);

		relative = (LinearLayout) findViewById(R.id.relative);
		new ASSL(AddCard.this, relative, 1134, 720, false);

		try {
			stripe = new Stripe(Data.STRIPE_API_KEY);
		} catch (AuthenticationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		month_spinner = new String[13];
		month_spinner[0] = "Select month";
		for (int i = 2; i <= 13; i++) {
			month_spinner[i - 1] = (i - 1) + "";
		}

		year_spinner = new String[41];
		year_spinner[0] = "Select year";
		for (int i = 1; i < 41; i++) {
			year_spinner[i] = (2014 + (i - 1)) + "";
		}

		favTitleText = (TextView) findViewById(R.id.favTitleText);

		cardNo = (TextView) findViewById(R.id.cardNoShow);
		cardNo.setTypeface(Data.regularFont(getApplicationContext()));
		cardText = (TextView) findViewById(R.id.cardText);
		cardText.setTypeface(Data.regularFont(getApplicationContext()));
		cardDivider = (ImageView) findViewById(R.id.cardDivider);
		// /cardDivider.setTypeface(Data.regularFont(getApplicationContext()));

		Spinner monthSpin = (Spinner) findViewById(R.id.monthSpinner);

		CustomAdapter<String> adapterMonthSpin = new CustomAdapter<String>(
				getApplicationContext(), android.R.layout.simple_spinner_item,
				month_spinner);
		adapterMonthSpin
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		monthSpin.setAdapter(adapterMonthSpin);

		Spinner yearSpin = (Spinner) findViewById(R.id.yearSpinner);

		CustomAdapter<String> adapterYearSpin = new CustomAdapter<String>(
				getApplicationContext(), android.R.layout.simple_spinner_item,
				year_spinner);
		adapterYearSpin
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		yearSpin.setAdapter(adapterYearSpin);

		monthSpin.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long id) {
				// TODO Auto-generated method stub
				// Toast.makeText(getBaseContext(),month_spinner[position],
				// Toast.LENGTH_SHORT).show();
				month.setText(month_spinner[position] + "");
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});
		yearSpin.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long id) {
				// TODO Auto-generated method stub
				// Toast.makeText(getBaseContext(),year_spinner[position],
				// Toast.LENGTH_SHORT).show();
				year.setText(year_spinner[position] + "");

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});

		cardNumber = (EditText) findViewById(R.id.cardno);
		cardNumber.setTypeface(Data.regularFont(getApplicationContext()));
		cvv = (EditText) findViewById(R.id.cvv);
		cvv.setTypeface(Data.regularFont(getApplicationContext()));
		month = (EditText) findViewById(R.id.month);
		month.setTypeface(Data.regularFont(getApplicationContext()));
		year = (EditText) findViewById(R.id.year);
		year.setTypeface(Data.regularFont(getApplicationContext()));

		signUpBtn = (Button) findViewById(R.id.signUpBtn);
		signUpBtn.setTypeface(Data.regularFont(getApplicationContext()));

		extraTextForScroll = (TextView) findViewById(R.id.extraTextForScroll);

		// cardNumber.addTextChangedListener(new TextWatcher() {
		//
		// @Override
		// public void afterTextChanged(Editable s) {
		// Log.w("", "input" + s.toString());
		//
		// if (s.length() > 0 && s.length() < 20 &&
		// !CODE_PATTERN.matcher(s).matches()) {
		// String input = s.toString();
		// String numbersOnly = keepNumbersOnly(input);
		// String code = formatNumbersAsCode(numbersOnly);
		//
		// Log.w("", "numbersOnly" + numbersOnly);
		// Log.w("", "code" + code);
		//
		//
		// cardNumber.removeTextChangedListener(this);
		// cardNumber.setText(code);
		// // You could also remember the previous position of the cursor
		// cardNumber.setSelection(code.length());
		// cardNumber.addTextChangedListener(this);
		// }
		// }
		//
		// @Override
		// public void beforeTextChanged(CharSequence s, int start, int count,
		// int after) {
		// }
		//
		// @Override
		// public void onTextChanged(CharSequence s, int start, int before, int
		// count) {
		// }
		//
		// private String keepNumbersOnly(CharSequence s) {
		// return s.toString().replaceAll("[^0-9]", ""); // Should of course be
		// more robust
		// }
		//
		// private String formatNumbersAsCode(CharSequence s) {
		// int groupDigits = 0;
		// String tmp = "";
		// int groupCheck=0;
		// for (int i = 0; i < s.length(); ++i) {
		// tmp += s.charAt(i);
		// ++groupDigits;
		// if (groupDigits == 4) {
		// groupCheck++;
		//
		// tmp += "-";
		// groupDigits = 0;
		// if(groupCheck==4)
		// {
		// tmp=tmp.substring(0,19);
		// }
		//
		// }
		// }
		// return tmp;
		// }
		// });
		//
		//
		//

		backBtn = (Button) findViewById(R.id.backBtn);

		backBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(R.anim.left_in, R.anim.left_out);
			}
		});

		cardNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				cardNumber.setError(null);
			}
		});

		cvv.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				cvv.setError(null);
			}
		});

		signUpBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				cardNumberString = cardNumber.getText().toString();
				cvvString = cvv.getText().toString();

				if ("".equalsIgnoreCase(cardNumberString)) {
					cardNumber.requestFocus();
					cardNumber.setError("Please enter card number");
					return;
				}

				if ("".equalsIgnoreCase(cvvString)) {
					cvv.requestFocus();
					cvv.setError("Please enter cvv");
					return;
				}

				if (!isNumeric(month.getText().toString())) {
					// month.setError("Please enter month");
					return;

				}

				if (!isNumeric(year.getText().toString())) {
					// year.setError("Please enter year");
					return;

				}

				Card card = new Card(cardNumberString, Integer.parseInt(month
						.getText().toString()), Integer.parseInt(year.getText()
						.toString()), cvvString);

				if (!card.validateNumber()) {
					cardNumber.requestFocus();
					cardNumber.setError("card number not valid");
					return;
				}
				if (!card.validateCVC()) {
					cvv.requestFocus();
					cvv.setError("cvc not valid");
					return;
				}

				DialogPopup.showLoadingDialog(AddCard.this, "Loading...");
				stripe.createToken(card, new TokenCallback() {
					public void onSuccess(Token token) {
						// Send token to your server
						DialogPopup.dismissLoadingDialog();
						Log.v("token = ", token.toString() + ",");
						// Toast.makeText(getApplicationContext(),
						// "Token = "+token.getId()+"", 5000).show();
						Data.stripeToken = token.getId().toString();

						AddCardServer(Data.stripeToken, token.getCard()
								.getLast4().toString());

					}

					public void onError(Exception error) {
						// Show localized error message
						DialogPopup.dismissLoadingDialog();
						Log.v("error = ", error.getMessage() + ",");

						new DialogPopup().alertPopup(AddCard.this, "",
								error.getMessage());
					}
				});

			}
		});

		final View activityRootView = findViewById(R.id.mainLinear);
		activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(
				new OnGlobalLayoutListener() {

					@Override
					public void onGlobalLayout() {
						Rect r = new Rect();
						// r will be populated with the coordinates of your view
						// that area still visible.
						activityRootView.getWindowVisibleDisplayFrame(r);

						int heightDiff = activityRootView.getRootView()
								.getHeight() - (r.bottom - r.top);
						if (heightDiff > 100) { // if more than 100 pixels, its
												// probably a keyboard...

							/************** Adapter for the parent List *************/

							ViewGroup.LayoutParams params_12 = extraTextForScroll
									.getLayoutParams();

							params_12.height = (int) (370.0f * ASSL.Yscale());

							extraTextForScroll.setLayoutParams(params_12);
							extraTextForScroll.requestLayout();

						} else {

							ViewGroup.LayoutParams params = extraTextForScroll
									.getLayoutParams();
							params.height = 0;
							extraTextForScroll.setLayoutParams(params);
							extraTextForScroll.requestLayout();

						}
					}
				});

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		if (Data.userData.last_4 != 0) {
			cardNo.setVisibility(0);
			cardText.setVisibility(0);
			cardDivider.setVisibility(0);
			cardNo.setText("xxxx-xxxx-xxxx-" + Data.userData.last_4);
			// signUpBtn.setText("Update");
			favTitleText.setText("Update Card");

		} else {
			cardNo.setVisibility(8);
			cardText.setVisibility(8);
			cardDivider.setVisibility(8);
			cardNo.setText("xxxx-xxxx-xxxx-" + Data.userData.last_4);
			// signUpBtn.setText("Add Card");
			favTitleText.setText("Add Card");
		}

	}

	/**
	 * Formats the watched EditText to a credit card number
	 */
	public static class FourDigitCardFormatWatcher implements TextWatcher {

		// Change this to what you want... ' ', '-' etc..
		private static final char space = ' ';

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			// Remove spacing char
			if (s.length() > 0 && (s.length() % 5) == 0) {
				final char c = s.charAt(s.length() - 1);
				if (space == c) {
					s.delete(s.length() - 1, s.length());
				}
			}
			// Insert char where needed.
			if (s.length() > 0 && (s.length() % 5) == 0) {
				char c = s.charAt(s.length() - 1);
				// Only if its a digit where there should be a space we insert a
				// space
				if (Character.isDigit(c)
						&& TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
					s.insert(s.length() - 1, String.valueOf(space));
				}
			}
		}
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_in, R.anim.left_out);
		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ASSL.closeActivity(relative);
		// System.gc();
	}

	public void AddCardServer(final String s_token, final String last_4) {
		if (AppStatus.getInstance(getApplicationContext()).isOnline(
				getApplicationContext())) {

			DialogPopup.showLoadingDialog(this, "Loading...");

			RequestParams params = new RequestParams();

			if (Data.locationFetcher != null) {
				Data.latitude = Data.locationFetcher.getLatitude();
				Data.longitude = Data.locationFetcher.getLongitude();
			}

			params.put("email", Data.getEmailAddress(getApplicationContext()));
			params.put("stripe_token", s_token);
			params.put("access_token", Data.userData.accessToken);
			params.put("last_4", last_4);

			Log.i("email", Data.getEmailAddress(getApplicationContext())
					+ ",,,");
			Log.i("stripe_token", s_token);
			Log.i("access_token", Data.userData.accessToken);
			Log.i("last_4", last_4);

			AsyncHttpClient client = new AsyncHttpClient();
			client.setTimeout(Data.SERVER_TIMEOUT);
			client.post(Data.SERVER_URL + "/add_credit_card", params,
					new AsyncHttpResponseHandler() {
						private JSONObject jObj;

						@Override
						public void onSuccess(String response) {
							Log.v("Server response", "response = " + response);

							try {
								jObj = new JSONObject(response);

								if (!jObj.isNull("error")) {

									int flag = jObj.getInt("flag");
									String errorMessage = jObj
											.getString("error");

									if (0 == flag) { // {"error": 'some
														// parameter
														// missing',"flag":0}//error
										new DialogPopup().alertPopup(
												AddCard.this, "", errorMessage);
									} else if (1 == flag) { // {"error":"email not  registered","flag":1}//error
										new DialogPopup().alertPopup(
												AddCard.this, "", errorMessage);
									} else if (2 == flag) { // {"error":"incorrect password","flag":2}//error
										new DialogPopup().alertPopup(
												AddCard.this, "", errorMessage);
									} else if (3 == flag) { // {"error":"enter otp","flag":2}//error

										new DialogPopup().alertPopup(
												AddCard.this, "", errorMessage);
									} else {
										new DialogPopup().alertPopup(
												AddCard.this, "", errorMessage);
									}
								} else {

									Data.userData.last_4 = Integer
											.parseInt(last_4);

									if (Data.userData.last_4 != 0) {
										cardNo.setVisibility(0);
										cardText.setVisibility(0);
										cardDivider.setVisibility(0);
										cardNo.setText("xxxx-xxxx-xxxx-"
												+ Data.userData.last_4);
										favTitleText.setText("Update Card");

										cardNumber.setText("");

										cvv.setText("");

										month.setText("");

										year.setText("");

									}
									new DialogPopup().alertPopup(AddCard.this,
											"", jObj.getString("log"));

								}
							} catch (Exception exception) {
								exception.printStackTrace();
								new DialogPopup().alertPopup(AddCard.this, "",
										Data.SERVER_ERROR_MSG);
							}

							DialogPopup.dismissLoadingDialog();
						}

						@Override
						public void onFailure(Throwable arg0) {
							Log.e("request fail", arg0.toString());
							DialogPopup.dismissLoadingDialog();
							new DialogPopup().alertPopup(AddCard.this, "",
									Data.SERVER_NOT_RESOPNDING_MSG);
						}
					});
		} else {
			new DialogPopup().alertPopup(AddCard.this, "",
					Data.CHECK_INTERNET_MSG);
		}

	}

	class CustomAdapter<T> extends ArrayAdapter<String> {
		public CustomAdapter(Context context, int textViewResourceId,
				String[] countryList) {

			super(context, textViewResourceId, countryList);
			// TextView tv=(TextView)findViewById(textViewResourceId);
			// tv.setTextSize(10);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View view = super.getView(position, convertView, parent);
			TextView textView = (TextView) view
					.findViewById(android.R.id.text1);

			textView.setText("");
			return view;
		}
	}

	public static boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

}
